﻿using Admin.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using System.Web.Security;
using Microsoft.AspNet.Identity;

namespace Admin.Controllers
{

	public class HomeController : Controller
	{

        ApplicationDbContext db = new ApplicationDbContext();

		[Authorize]
		public ActionResult Index()
		{

            var getCurrentUserId = User.Identity.GetUserId();

            var getUser = db.Users.Where(x=>x.Id == getCurrentUserId).FirstOrDefault();

            //string firstname = getUser.Firstname;

            //ViewBag.firstname = firstname;
			return View(getUser);
		}

		[Authorize(Roles = "Administrator, Agent")]
		public ActionResult Dashboard()
		{
			return View();
		}

		[Authorize(Roles = "Administrator, Agent")]
		public ActionResult About()
		{
			ViewBag.Message = "Your application description page.";

			return View();
		}


	}
}