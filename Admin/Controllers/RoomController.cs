﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Admin.Models;

namespace Admin.Controllers
{
    public class RoomController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Room
        public ActionResult Index()
        {
            var roomModels = db.RoomModels.Include(r => r.applicationuser).Include(r => r.roonlist).Include(r => r.section).Include(r => r.subject);
            return View(roomModels.ToList());
        }

        // GET: Room/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RoomModel roomModel = db.RoomModels.Find(id);
            if (roomModel == null)
            {
                return HttpNotFound();
            }
            return View(roomModel);
        }

        // GET: Room/Create
        public ActionResult Create()
        {
            ViewBag.usersName = new SelectList(db.Users, "Id", "Firstname");
            ViewBag.roomName = new SelectList(db.RoomLists, "id", "Name");
            ViewBag.SectionDescription = new SelectList(db.SectionModels, "Id", "sectionName");
            ViewBag.SubjectName = new SelectList(db.SubjectModels, "Id", "SubjectName");
            return View();
        }

        // POST: Room/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,roomName,Description,yearlevel,SectionDescription,usersName,SubjectName")] RoomModel roomModel)
        {
            if (ModelState.IsValid)
            {
                db.RoomModels.Add(roomModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.usersName = new SelectList(db.Users, "Id", "Firstname", roomModel.usersName);
            ViewBag.roomName = new SelectList(db.RoomLists, "id", "Name", roomModel.roomName);
            ViewBag.SectionDescription = new SelectList(db.SectionModels, "Id", "sectionName", roomModel.SectionDescription);
            ViewBag.SubjectName = new SelectList(db.SubjectModels, "Id", "SubjectName", roomModel.SubjectName);
            return View(roomModel);
        }

        // GET: Room/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RoomModel roomModel = db.RoomModels.Find(id);
            if (roomModel == null)
            {
                return HttpNotFound();
            }
            ViewBag.usersName = new SelectList(db.Users, "Id", "Firstname", roomModel.usersName);
            ViewBag.roomName = new SelectList(db.RoomLists, "id", "Name", roomModel.roomName);
            ViewBag.SectionDescription = new SelectList(db.SectionModels, "Id", "sectionName", roomModel.SectionDescription);
            ViewBag.SubjectName = new SelectList(db.SubjectModels, "Id", "SubjectName", roomModel.SubjectName);
            return View(roomModel);
        }

        // POST: Room/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,roomName,Description,yearlevel,SectionDescription,usersName,SubjectName")] RoomModel roomModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(roomModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.usersName = new SelectList(db.Users, "Id", "Firstname", roomModel.usersName);
            ViewBag.roomName = new SelectList(db.RoomLists, "id", "Name", roomModel.roomName);
            ViewBag.SectionDescription = new SelectList(db.SectionModels, "Id", "sectionName", roomModel.SectionDescription);
            ViewBag.SubjectName = new SelectList(db.SubjectModels, "Id", "SubjectName", roomModel.SubjectName);
            return View(roomModel);
        }

        // GET: Room/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RoomModel roomModel = db.RoomModels.Find(id);
            if (roomModel == null)
            {
                return HttpNotFound();
            }
            return View(roomModel);
        }

        // POST: Room/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RoomModel roomModel = db.RoomModels.Find(id);
            db.RoomModels.Remove(roomModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
