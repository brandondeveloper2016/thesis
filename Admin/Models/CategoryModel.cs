﻿using Admin.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Admin.Models
{
    public class CategoryModel
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public string VideoURL { get; set; }
    }

    public class SectionModel
    {

        public int  Id { get; set; }
        public string sectionName { get; set; }
        public string SectionDescription { get; set; }
        public virtual ICollection<RoomModel> room { get; set; }
    }

    public class RoomList
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<RoomModel> room { get; set; }
    }

    public class RoomModel
    {
        public int Id { get; set; }
        [ForeignKey("roonlist")]
        public int roomName { get; set; }

        public string Description { get; set; }
        public YearLevel yearlevel { get; set; } 

        [ForeignKey("section")]
        public int SectionDescription { get; set; }

        [ForeignKey("applicationuser")]
        public string usersName { get; set; }

        [ForeignKey("subject")]
        public int SubjectName { get; set; }

        public virtual SectionModel section { get; set; }
        public virtual ApplicationUser applicationuser { get; set; }
        public virtual SubjectModel subject { get; set; }
        public virtual RoomList roonlist { get; set; }

    }

    public enum YearLevel
    {
       Grade1, Grade2, Grade3, Grade4, Grade5, Grade6, Grade7, Grade8, Grade9, Grade10, Grade11, Grade12
    }

    public class SubjectModel
    {
        public int Id { get; set; }
        public string SubjectName { get; set; }
        public string SubjectDescription { get; set; }
        public YearLevel yearlevel { get; set; }

        public virtual ICollection<RoomModel> room { get; set; }
    }
    //--------------------------------------------------------------------------------------------------------------
    public class QuestionModel
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public int SubjectId { get; set; }
        [AllowHtml]
        public string QuestionDesc { get; set; }
        public string Answer { get; set; }
        public string Choice1 { get; set; }
        public string Choice2 { get; set; }
        public string Choice3 { get; set; }
    }

    public class ExamModel
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public int SubjectId { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        public int TotalItems { get; set; }
    }

    public class ExamItemsModel
    {
        public int Id { get; set; }
        public ExamModel ExamInfo { get; set; }
        public List<QuestionModel> randomQuestions { get; set; }
    }


    public class ExamResultModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        List<ItemAnswerModel> QuestionResult { get; set; }
    }


    public class ItemAnswerModel
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public string UserAnswer { get; set; }
        public int Result { get; set; }
    }

    public class DefaultConnection : DbContext
    {
        public DbSet<CategoryModel> tbl_Category { get; set; }
        public DbSet<QuestionModel> tbl_Question { get; set; }
        public DbSet<ExamModel> tbl_Exam { get; set; }
        public DbSet<ExamResultModel> tbl_Score { get; set; }
        public DbSet<SubjectModel> tbl_Subject { get; set; }
        public DbSet<RoomModel> tbl_Room { get; set; }
        public DbSet<ExamItemsModel> tbl_ExamItems { get; set; }
        public DbSet<SectionModel> tbl_Section{ get; set; }
    }
}